import { Injectable } from '@angular/core';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Vedio } from './../model/vedio';
import { HttpErrorHandler, HandleError } from './../core/http-error-handler.service';
import { HttpService } from './http.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable()
export class VediosService {
  vediosUrl = "http://localhost:3001/vedio";  // URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpService,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('VedioesService');
  }

  /** GET heroes from the server */
  getVedios (): Observable<Vedio[]> {
    return this.http.get(this.vediosUrl) as Observable<Vedio[]> ;
  }
  searchVedio(id: number): Observable<Vedio> {
    const url = this.vediosUrl+"/"+id;
    console.log("url:"+url)
    return  this.http.get(url);
  }
  /** POST: add a new hero to the database */
  addVedio (vedio: Vedio): Observable<Vedio> {
    return this.http.post(this.vediosUrl, vedio) as Observable<Vedio> ;
  }

  /** DELETE: delete the hero from the server */
  deleteVedio (id: number): Observable<{}> {
    const url = `${this.vediosUrl}/${id}`; // DELETE api/heroes/42
    return this.http.delete(url);
  }

  /** PUT: update the hero on the server. Returns the updated hero upon success. */
  updateVedio (vedio: Vedio): Observable<Vedio> {
    console.log("id"+vedio.id+"like:"+vedio.like+"unlike:"+vedio.unLike)
    return this.http.put(this.vediosUrl+"/"+vedio.id, vedio) as Observable<Vedio>;
  }
}
