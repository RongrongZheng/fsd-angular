import { Component, OnInit,ElementRef } from '@angular/core';
import { Vedio } from './../../model/vedio';
import { VediosService} from './../../core/vedios.service';
import { Menu } from './../../model/menu';
@Component({
  selector: 'app-play-list',
  templateUrl: './play-list.component.html',
  styleUrls: ['./play-list.component.css']
})
export class PlayListComponent implements OnInit {

  constructor(
    private el: ElementRef,
    private vedioService:VediosService,
  ) { }

  ngOnInit(): void {
    this.getMenus();
  }
  getMenus(): void {
      
    this.vedioService.getVedios().subscribe(vedios => {
      let i = 0;
      for (let vedio of vedios) {
        let disabled = true;
        if(vedio.status == 1) disabled = false;
        this.menus[1].children[i]= new Menu(2,vedio.title,'/video/'+vedio.id,'play-circle',false,disabled);
        i++;
      }
    })
  }

  mode = false;
  dark = false;
  menus = [
    {
      level: 1,
      title: 'Vedio',
      url: '/edit',
      icon: 'setting',
      selected: false,
      disabled: false
    },
    {
      level: 1,
      title: 'My Vedios',
      icon: 'video-camera',
      open: true,
      selected: false,
      disabled: false,
      children: [ ]
    }
  ];

}
