import { Component, OnInit  } from '@angular/core';
import {Vedio} from './../../model/vedio'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import{ VediosService } from './../../core/vedios.service'


@Component({
  selector: 'app-edit-vedio',
  templateUrl: './edit-vedio.component.html',
  styleUrls: ['./edit-vedio.component.css']
})
export class EditVedioComponent implements OnInit {

  playList: Vedio[];
  editCache: { [key: number]: { edit: boolean; vedio: Vedio } } = {};

  isVisible = false;
  isConfirmLoading = false;

  constructor( 
    private fb: FormBuilder,
    private vediosService: VediosService) { }

  ngOnInit(): void {
    
    this.getPlayList();
    this.initValidateForm(); 
  }
  getPlayList(){
    this.vediosService.getVedios().subscribe(
      res => {this.playList = res ;
      this.updateEditCache();
      }
    );
  }
  initValidateForm(){
    this.validateForm = this.fb.group({
      title: [null, [Validators.required]],
      url: [null, [Validators.required]],
      remember: [true]
    });
  }

  startEdit(id: string): void {
    this.editCache[id].edit = true;
  }

  cancelEdit(id: number): void {
    this.editCache[id] = {
      vedio: { ...this.playList[id] },
      edit: false
    };
  }

  saveEdit(id: number): void {

    let vedio = this.editCache[id].vedio;
    vedio.status = 0;
    this.vediosService.updateVedio(vedio).subscribe(
      data =>{
        //this.updateEditCache();
        Object.assign(this.playList[id], this.editCache[id].vedio);
        this.editCache[id].edit = false;
    }
    );
   
  }

  updateEditCache(): void {
    this.playList.forEach(item => {
      this.editCache[item.id] = {
        edit: false,
        vedio: { ...item }
      };
    });
  }
  
  Approve(id: number): void {
    let vedio = this.editCache[id].vedio;
    vedio.status=1;
    this.vediosService.updateVedio(vedio).subscribe(
      data =>{
         //console.log("put approve"+data);
         Object.assign(this.playList[id], this.editCache[id].vedio);
         this.editCache[id].edit = false;
    }
    );
  }

  delete(id: number): void {
    this.vediosService.deleteVedio(id).subscribe(
      data =>{
        this.getPlayList();
    });
  }
  
  showModal(): void {
    this.isVisible = true;
  }

  addVedio(): void {
    let vedio: Vedio = new Vedio(
      this.playList.length,
      this.validateForm.value.title,
      this.validateForm.value.url,
      0,0,0) ;
    this.vediosService.addVedio(vedio).subscribe(data =>{
      this.getPlayList();
    })

    this.isConfirmLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
    }, 1000);
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  validateForm: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

}


