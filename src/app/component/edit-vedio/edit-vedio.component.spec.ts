import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVedioComponent } from './edit-vedio.component';

describe('EditVedioComponent', () => {
  let component: EditVedioComponent;
  let fixture: ComponentFixture<EditVedioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVedioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVedioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
