import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyVedioComponent } from './my-vedio.component';

describe('MyVedioComponent', () => {
  let component: MyVedioComponent;
  let fixture: ComponentFixture<MyVedioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyVedioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyVedioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
