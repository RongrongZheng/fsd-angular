import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ControlComponent } from './component/control/control.component';
import { PlayListComponent } from './component/play-list/play-list.component';
import { MyVedioComponent } from './component/my-vedio/my-vedio.component';
import { EditVedioComponent } from './component/edit-vedio/edit-vedio.component';

import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';

import { NgZorroAntdModule  } from 'ng-zorro-antd';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VediosService } from './core/vedios.service'
import { HttpService } from './core/http.service';
import { HttpErrorHandler} from './core/http-error-handler.service';
import { MessageService} from './core/message.service'
@NgModule({
  declarations: [
    AppComponent,
    ControlComponent,
    PlayListComponent,
    MyVedioComponent,
    EditVedioComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgZorroAntdModule ,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'My-Xsrf-Cookie',
      headerName: 'My-Xsrf-Header',
    }),
    AppRoutingModule
  ],
  providers: [VediosService,HttpService,HttpErrorHandler,MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
