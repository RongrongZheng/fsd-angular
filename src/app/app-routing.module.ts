import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyVedioComponent } from './component/my-vedio/my-vedio.component';
import { EditVedioComponent } from './component/edit-vedio/edit-vedio.component';

const routes: Routes = [
  
  { path: 'video/:id', component: MyVedioComponent},
  { path: 'edit', component: EditVedioComponent},
  { path: '**', component: MyVedioComponent}
 
];

@NgModule({

  imports: [
      RouterModule.forRoot(
          routes,
          { enableTracing: true } // <-- debugging purposes only
        )
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
